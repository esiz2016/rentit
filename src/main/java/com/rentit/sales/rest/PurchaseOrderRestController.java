package com.rentit.sales.rest;

import com.rentit.sales.application.dto.PurchaseOrderDTO;
import com.rentit.sales.application.service.SalesService;
import com.rentit.sales.domain.model.PurchaseOrderID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import java.net.URI;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/api/sales/orders")
public class PurchaseOrderRestController {
    @Autowired
    SalesService salesService;

    @RequestMapping(method = GET, path = "/hello")
    public String message(){
    	
    	return "Hello";
    }
    
    @RequestMapping(method = POST, path = "/orders")
    public /*ResponseEntity<PurchaseOrderDTO>*/ String createPurchaseOrder(@RequestBody PurchaseOrderDTO poDTO) throws Exception {
          return "Hey Antom";
    	/*   poDTO = salesService.createPurchaseOrder(poDTO);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(new URI(poDTO.getId().getHref()));
        return new ResponseEntity<PurchaseOrderDTO>(poDTO, headers, HttpStatus.CREATED); */
    }

    @RequestMapping(method = DELETE, path = "/orders/{oid}/accept")
    public ResponseEntity<PurchaseOrderDTO> RejectingPO( @PathVariable( "oid") Long id) throws Exception {
         salesService.RejectPurchaseOrder(PurchaseOrderID.of(id));     
         
         PurchaseOrderDTO poDTO = salesService.getPurchaseOrder(PurchaseOrderID.of(id));
         HttpHeaders headers = new HttpHeaders();
         headers.setLocation(new URI(poDTO.getId().getHref()));
         return new ResponseEntity<PurchaseOrderDTO>(poDTO, headers, HttpStatus.CREATED);
         
    }
    
    @RequestMapping(method = PUT, path = "/orders/{oid}")
    public ResponseEntity<PurchaseOrderDTO> PendingConfirmationPO( @PathVariable("oid") Long id) throws Exception {
         salesService.PendingPurchaseOrder(PurchaseOrderID.of(id));       
    
         PurchaseOrderDTO poDTO = salesService.getPurchaseOrder(PurchaseOrderID.of(id));
         HttpHeaders headers = new HttpHeaders();
         headers.setLocation(new URI(poDTO.getId().getHref()));
         return new ResponseEntity<PurchaseOrderDTO>(poDTO, headers, HttpStatus.CREATED);
    
    
    }
    
    @RequestMapping(method = POST, path = "/orders/{oid}/accept")
    public ResponseEntity<PurchaseOrderDTO> OpenningPO( @PathVariable("oid") Long id) throws Exception {
         salesService.PendingPurchaseOrder(PurchaseOrderID.of(id));       
   
         PurchaseOrderDTO poDTO = salesService.getPurchaseOrder(PurchaseOrderID.of(id));
         HttpHeaders headers = new HttpHeaders();
         headers.setLocation(new URI(poDTO.getId().getHref()));
         return new ResponseEntity<PurchaseOrderDTO>(poDTO, headers, HttpStatus.CREATED);
    }
    
    @RequestMapping(method = DELETE, path = "/orders/{oid}")
    public ResponseEntity<PurchaseOrderDTO> ClosingPO( @PathVariable( "oid") Long id) throws Exception {
         salesService.ClosePurchaseOrder(PurchaseOrderID.of(id));       
    
         PurchaseOrderDTO poDTO = salesService.getPurchaseOrder(PurchaseOrderID.of(id));
         HttpHeaders headers = new HttpHeaders();
         headers.setLocation(new URI(poDTO.getId().getHref()));
         return new ResponseEntity<PurchaseOrderDTO>(poDTO, headers, HttpStatus.CREATED);
    
    
    
    }
    
    
    @RequestMapping(method = POST, path = "/orders/{oid}/extention")
    public ResponseEntity<PurchaseOrderDTO> PendingExtentionPO(@PathVariable( "oid") Long id) throws Exception {
         salesService.ClosePurchaseOrder(PurchaseOrderID.of(id));    
         
         PurchaseOrderDTO poDTO = salesService.getPurchaseOrder(PurchaseOrderID.of(id));
         HttpHeaders headers = new HttpHeaders();
         headers.setLocation(new URI(poDTO.getId().getHref()));
         return new ResponseEntity<PurchaseOrderDTO>(poDTO, headers, HttpStatus.CREATED);
         
         
         
         
    }
    
    
    
    
    
    
    
    @RequestMapping(method = POST, path = "/{id}")
    public PurchaseOrderDTO showPurchaseOrder(@PathVariable Long id) throws Exception {
        PurchaseOrderDTO poDTO = salesService.findPurchaseOrder(PurchaseOrderID.of(id));
        return poDTO;
    }
    
    
    @ExceptionHandler(BindException.class)
    @ResponseStatus(HttpStatus.PRECONDITION_FAILED)
    public String bindExceptionHandler(Exception ex) {
        return ex.getMessage();
    }
    
}
