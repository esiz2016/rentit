package com.rentit.sales.domain.repository;

import com.rentit.sales.domain.model.PurchaseOrder;
import com.rentit.sales.domain.model.PurchaseOrderID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, PurchaseOrderID> {
    
	@Query(value ="update PURCHASE_ORDER   set status='REJECTED'   where id=?1", nativeQuery = true)
	void RejectPO(PurchaseOrderID id);
	
	@Query(value ="update PURCHASE_ORDER   set status='PENDING'   where id=?1", nativeQuery = true)
	void PendingPO(PurchaseOrderID id);
	
	@Query(value ="update PURCHASE_ORDER   set status='CLOSED'   where id=?1", nativeQuery = true)
	void ClosePO(PurchaseOrderID of);
	
	@Query(value ="SELECT * FROM PURCHASE_ORDER where id=?1", nativeQuery = true)
	PurchaseOrder getPO(Long id);
}
