package com.rentit;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@EntityScan(basePackageClasses = { RentitRefApplication.class, Jsr310JpaConverters.class })
@SpringBootApplication
public class RentitRefApplication {
	@Configuration
	static class RestTemplateConfiguration {
		@Autowired
		ObjectMapper mapper;
		@Bean
		public RestTemplate restTemplate() {
			RestTemplate _restTemplate = new RestTemplate();
			List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			_restTemplate.setMessageConverters(messageConverters);
			return _restTemplate;
		}
	}

	public static void main(String[] args) {
		SpringApplication.run(RentitRefApplication.class, args);
	}
}
